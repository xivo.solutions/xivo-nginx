xivo-nginx
==========

xivo-nginx is a Debian package that provides a basic configuration for the
nginx server used by XiVO.
